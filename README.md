# **TFG**
## Trabajo Fin de Carrera Ingeniería en Diseño Industrial y Desarrollo de Producto 2016-2020
### Algoritmo convertidor de nudos Pretzel en nudos tipo trenza (Braid)
#### Ángel del Pozo Manglano

Para este TFG se ha realizado un estudio sobre la teoría de nudos, una de las ramas de la topología.
Según se ha ido desarrollando se ha ido centrando en los nudos Pretzel, y cómo convertirlos en nudos trenza.

El programa puede probarse online desde http://adelpozoman.es/tfg


