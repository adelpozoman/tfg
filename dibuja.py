def pretzeldraw(pretzel):
    print("╭───────────╮")
    print("│  __   __  │")
    altura = int(max(pretzel))
    if abs(min(pretzel)) > altura:
        altura = int(abs(min(pretzel)))
    for i in range(0, int(altura)):
        print("\ /  \ /  \ /")
        if pretzel[0] < 0 and i < abs(pretzel[0]):
            print(" \\ ", end='')
        elif pretzel[0] > 0 and i < abs(pretzel[0]):
            print(" / ", end='')
        else:
            print("│ │", end='')
        print("  ", sep='', end='')
        if pretzel[1] < 0 and i < abs(pretzel[1]):
            print(" \\ ", end='')
        elif pretzel[1] > 0 and i < abs(pretzel[1]):
            print(" / ", end='')
        else:
            print("│ │", end='')
        print("  ", sep='', end='')
        if pretzel[2] < 0 and i < abs(pretzel[2]):
            print(" \\")
        elif pretzel[2] > 0 and i < abs(pretzel[2]):
            print(" /")
        else:
            print("│ │")
        print("/ \  / \  / \\")

    print("│  ͞ ͞    ͞ ͞   │")
    print("╰───────────╯")


def braiddraw(braid):
    ancho = int(max(braid))+1
    if abs(min(braid))+1 > ancho:
        ancho = int(abs(min(braid)))+1
    for i in range(0, ancho*2-1):
        if i % 2 == 0:
            print('─', end='')
            for j in range(0, len(braid)):
                if abs(braid[j]) == ancho-i/2-1 or int(abs(braid[j])) == ancho-i/2:
                    if braid[j] < 0:
                        print(" ─", end='')
                    elif braid[j] > 0:
                        print(" ─", end='')
                else:
                    print("──", end='')

        if i % 2 == 1:
            print(' ', end='')
            for j in range(0, len(braid)):
                if abs(braid[j]) == ancho-i//2-1:
                    if braid[j] < 0:
                        print("╱ ", end='')
                    elif braid[j] > 0:
                        print("╲ ", end='')
                else:
                    print("  ", end='')
        print("")
