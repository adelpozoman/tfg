def pri(n): return int(abs(n)//2)


def lectura(pretzel):
    while True:
        str = input("Inserta los valores de tu nudo Pretzel:")
        str = str.replace('.', ',').replace(
            '+', ',').replace(' ', ',').split(',')
        try:
            for i in str:
                pretzel.append(int(i))
            if len(pretzel) % 2 == 0 or len(pretzel) == 3 or len(pretzel) == 1:
                return
            else:
                print("No puedo procesar un Pretzel de {} columnas, vuelve a intentarlo".format(
                    len(pretzel)))
                pretzel.clear()
        except ValueError:
            print("No me has dado números")
            pretzel.clear()


def pregunta(quest):
    while True:
        ok = input(quest+' ')
        if ok in ('Sí', 'Si', 'si', 'sí', 'SI', 'sI', 'S', 'yes', 'Yes', 'sure', 'roger', '1', 'Continuar', 'continuar', 'claro', 'Activar', 'activar', 'activa'):
            return True
        if ok in ('No', 'n', 'no', 'nO', 'nop', 'nope', 'Negative', '0', 'Salir', 'salir'):
            return False
        print("No has escogido una opción válida")


def pares(pretzel):
    cont = 0
    for i in range(0, len(pretzel)):
        if pretzel[i] % 2 == 0:
            cont += 1
    return cont


def imp_par_imp(pretzel, braid):
    for i in range(1, pri(pretzel[1])+1):
        braid.append(-i-1) if pretzel[1] > 1 else braid.append(i+1)
    for i in range(0, abs(pretzel[0])):
        braid.append(1) if pretzel[0] > 0 else braid.append(-1)
    for i in range(1, pri(pretzel[1])):
        braid.append(i+1) if pretzel[1] > 0 else braid.append(-i-1)
    for i in reversed(range(1, pri(pretzel[1])+1)):
        braid.append(-i-1) if pretzel[1] > 0 else braid.append(i+1)
    for i in range(0, abs(pretzel[2])):
        braid.append(1) if pretzel[2] > 0 else braid.append(-1)


def par_imp_imp(pretzel, braid):
    for i in range(0, pri(pretzel[0])):
        braid.append(-i-1) if pretzel[0] > 0 else braid.append(i+1)
    for i in range(0, abs(pretzel[1])):
        braid.append(
            pri(pretzel[0])+1) if pretzel[1] > 0 else braid.append(-pri(pretzel[0])-1)
    for i in reversed(range(0, pri(pretzel[0]))):
        braid.append(-i-1) if pretzel[0] > 0 else braid.append(i+1)
    for i in range(2, pri(pretzel[0])+1):
        braid.append(i) if pretzel[0] > 0 else braid.append(-i)
    for i in range(0, abs(pretzel[2])):
        braid.append(
            pri(pretzel[0])+1) if pretzel[2] > 0 else braid.append(-pri(pretzel[0])-1)


def imp_par_par(pretzel, braid):
    for i in range(0, pri(pretzel[2])):
        braid.append(-i-1) if pretzel[2] > 0 else braid.append(i+1)
    for i in range(0, abs(pretzel[0])):
        braid.append(
            pri(pretzel[2])+1) if pretzel[0] > 0 else braid.append(-pri(pretzel[2])-1)
    for i in reversed(range(0, pri(pretzel[2]))):
        braid.append(-i-1) if pretzel[2] > 0 else braid.append(i+1)
    for i in range(2, pri(pretzel[2])+1):
        braid.append(i) if pretzel[2] > 0 else braid.append(-i)
    for i in range(0, abs(pretzel[1])):
        braid.append(
            pri(pretzel[2])+1) if pretzel[1] > 0 else braid.append(-pri(pretzel[2])-1)


def imp_imp_imp(pretzel, braid):
    for i in range(1, pri(pretzel[0])+2):
        braid.append(-i) if pretzel[0] > 0 else braid.append(i)
    for i in reversed(range(2, pri(pretzel[0])+2)):
        braid.append(-i) if pretzel[0] > 0 else braid.append(i)
    # A2
    for i in range(pri(pretzel[1])):
        for j in range(-(2+pri(pretzel[0])+i), -2-i):
            braid.append(j)
    # A3
    for i in range(pri(pretzel[2])):
        for j in range(2+pri(pretzel[1])+pri(pretzel[0])+i, 2+pri(pretzel[1])+1+i-1, -1):
            braid.append(j)
    # A4
    for i in range(pri(pretzel[2])):
        for j in range(2+pri(pretzel[1])+i, 2+i, -1):
            braid.append(j)
    # A5
    for i in range(pri(pretzel[2])):
        braid.append(i+2) if pretzel[2] > 0 else braid.append(-i-2)
    # A6, casi equivalente a A1
    for i in range(1, pri(pretzel[2])+2):
        braid.append(-i) if pretzel[2] > 0 else braid.append(i)
    for i in reversed(range(2, pri(pretzel[2])+2)):
        braid.append(-i) if pretzel[2] > 0 else braid.append(i)
    # A7
    for i in range(pri(pretzel[1])):
        for j in range(-2-pri(pretzel[2])-i, -2-i):
            braid.append(j)
    # A8
    for i in range(pri(pretzel[1])):
        braid.append(i+2) if pretzel[1] > 0 else braid.append(-i-2)
    # A9
    for i in range(1, pri(pretzel[1])+2):
        braid.append(-i) if pretzel[1] > 0 else braid.append(i)
    for i in reversed(range(2, pri(pretzel[1])+2)):
        braid.append(-i) if pretzel[1] > 0 else braid.append(i)
    # A10
    for i in range(pri(pretzel[0])):
        for j in range(pri(pretzel[2])):
            braid.append(-2-pri(pretzel[1])-pri(pretzel[2])-i+j)
    # A11
    for i in range(pri(pretzel[0])):
        for j in range(2+pri(pretzel[1])+i, 2+i, -1):
            braid.append(j)
    # A12
    for i in range(pri(pretzel[0])):
        braid.append(i+2) if pretzel[0] > 0 else braid.append(-i-2)


def doble(pretzel, braid):
    for dummy in range(0, abs(pretzel[0])):
        braid.append(1) if pretzel[0] > 0 else braid.append(-1)
    for dummy in range(0, abs(pretzel[1])):
        braid.append(1) if pretzel[1] > 0 else braid.append(-1)


def cuadru(pretzel, braid):
    for i in range(1, int((len(pretzel)/2))):
        braid.extend((-2*i, i*2-1, 2*i))
    for i in reversed(range(0+1, len(pretzel)+1, 2)):
        for dummy in range(abs(pretzel[len(pretzel)-i-1])):
            braid.append(i) if pretzel[len(pretzel) -
                                       i-1] > 0 else braid.append(-i)
    for i in reversed(range(1, int(len(pretzel)/2))):
        braid.extend((-2*i, -2*i+1, 2*i))
    for i in range(0, len(pretzel), 2):
        for dummy in range(abs(pretzel[1+i])):
            braid.append(
                len(pretzel)-1-i) if pretzel[i+1] > 0 else braid.append(-(len(pretzel)-1-i))
