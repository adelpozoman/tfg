from funciones import pri, pares, pregunta, lectura, imp_par_imp, par_imp_imp, imp_imp_imp, imp_par_par, doble, cuadru
from dibuja import pretzeldraw, braiddraw

print("Bienvenido, ¡esto es ThermoBraid!")

grafico = 0
while True:
    pretzel = []
    braid = []
    lectura(pretzel)
    print("Tu nudo Pretzel es:", pretzel)
    if grafico == 1 and len(pretzel) == 3:
        pretzeldraw(pretzel)

    if len(pretzel) == 1:
        print("¡El nudo trivial!")
        braid = [0]
    elif len(pretzel) == 2:
        doble(pretzel, braid)
    elif len(pretzel) == 3:
        if int(pares(pretzel)) == 2:
            if pretzel[1] % 2 != 0:
                par_imp_imp(pretzel, braid)
            elif pretzel[0] % 2 != 0:
                imp_par_par(pretzel, braid)
            elif pretzel[2] % 2 != 0:
                imp_par_par(list(reversed(pretzel)), braid)
        elif int(pares(pretzel)) == 3:
            imp_par_imp(pretzel, braid)
        elif int(pares(pretzel)) == 1:
            if pretzel[0] % 2 == 0:
                par_imp_imp(pretzel, braid)
            elif pretzel[1] % 2 == 0:
                imp_par_imp(pretzel, braid)
            elif pretzel[2] % 2 == 0:
                par_imp_imp(list(reversed(pretzel)), braid)
        elif int(pares(pretzel)) == 0:
            imp_imp_imp(pretzel, braid)
    elif len(pretzel) % 2 == 0:
        cuadru(pretzel, braid)
    else:
        print('error')
    print("\nSi lo transformamos en trenza, tiene {} cruce{}, {} cuerda{} y vale:\n{}".format(
        len(braid), 's' if len(braid) > 1 else '', max(braid)+1 if abs(min(braid)) < max(braid) else abs(min(braid))+1, 's' if len(braid) > 1 else '', braid))
    if grafico == 1 and (braid != None or braid != [0]):
        braiddraw(braid)
    print("")

    if pregunta("¿Quieres introducir otro nudo Pretzel?(si/no)") != 1:
        break
    if pregunta("¿Quieres visualizar el nudo?(si/no)") != grafico:
        if grafico == 0:
            grafico = 1
            print("Visualización activada")
        elif grafico == 1:
            grafico = 0
            print("Visualización desactivada")
